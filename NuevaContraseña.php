<html>
    <head>
        <meta charset="UTF-8">
        <title>Modificar perfil</title>
    </head>
    <body>
        <?php
        session_start();
        require_once 'bbdduser.php';
        if (isset($_SESSION["username"])) {
            $username = $_SESSION["username"];
            $passactual = $_SESSION["password"];
            if (isset($_POST["comprobar"])) {
                $pass = $_POST["pass"];
                if ($passactual == $pass) {
                    echo "<p><a href = 'NuevaContraseña.php'>Cambiar Contraseña</a></p>";
                } else {
                   
                }
            } else {
                // Traemos los datos actuales del usuario
                $datos = getUser($username);
                $fila = mysqli_fetch_array($datos);
                extract($fila);
                echo "<form action='' method='POST'>";
                echo "<p>Perfil de $username</p>";
                echo "<p>Password: <input type='password' name='pass'></p>";
                echo "<p><input type='submit' name='comprobar' value='Modificar'></p>";
                echo "</form>";
            }
        } else {
            echo "Usuario no autentificado";
        }
        ?>
    </body>
</html>