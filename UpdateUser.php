<!DOCTYPE html>
<!--  Formulario para modificar perfil del usuario -->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Modificar perfil</title>
    </head>
    <body>
        <?php
        session_start();
        require_once 'bbdduser.php';
        if (isset($_SESSION["username"])) {
            $username = $_SESSION["username"];
            if (isset($_POST["comprobar"])) {
                $passactual = $_SESSION["password"];
                $pass = $_POST["pass"];
                if ($passactual == $pass) {
                    echo "<p><a href = 'NuevaContraseña.php'>Cambiar Contraseña</a></p>";
                } else {
                    echo "Contraseña no coincide con la Actual";
                    echo "<p><a href = 'UpdateUser.php'>Modificar Contraseña</a></p>";
                }
            } else {
                // Traemos los datos actuales del usuario
                echo "<form action='' method='POST'>";
                echo "<p>Perfil de $username</p>";
                echo "<p>Password: <input type='password' name='pass'></p>";
                echo "<p><input type='submit' name='comprobar' value='Modificar'></p>";
                echo "</form>";
            }
        } else {
            echo "Usuario no autentificado";
        }
        ?>
    </body>
</html>
