<?php

require_once 'bbdd.php';

function updateUser($username, $pass, $email) {
    $con = conectar("royal");
    $update = "update user set password='$pass', email='$email' where username='$username'";
    if (mysqli_query($con, $update)) {
        echo "<p>Datos del perfil modificiados</p>";
    } else {
        echo mysqli_error($con);
    }
    desconectar($con);
}

function getUser($username) {
    $con = conectar("royal");
    $query = "select * from user where username='$username'";
    $resultado = mysqli_query($con, $query);
    desconectar($con);
    return $resultado;
}

function getTipoUsuario($username) {
    $con = conectar("royal");
    $query = "select type from user where username='$username'";
    $resultado = mysqli_query($con, $query);
    $fila = mysqli_fetch_array($resultado);
    extract($fila);
    desconectar($con);
    return $type;
}

function verificarUser($username, $password) {
    $con = conectar("royal");
    $query = "select * from user where username='$username' and password='$password'";
    $resultado = mysqli_query($con, $query);
    $filas = mysqli_num_rows($resultado);
    desconectar($con);
    if ($filas > 0) {
        return true;
    } else {
        return false;
    }
}

function insertUser($nusuario, $pass, $type, $win, $level) {
    $conexion = conectar("royal");
    $insert = "insert into user values "
            . "('$nusuario', '$pass', $type, $win, $level)";
    if (mysqli_query($conexion, $insert)) {
        echo "<p>Usuario dado de alta</p>";
    } else {
        echo mysqli_error($conexion);
    }
    desconectar($conexion);
}

function insertCard($nombre, $tipo, $calidad, $salud, $daño, $coste) {
    $conexion = conectar("royal");
    $insert = "insert into card values "
            . "('$nombre', '$tipo', '$calidad', $salud, $daño, $coste)";
    if (mysqli_query($conexion, $insert)) {
        echo "<p>Usuario dado de alta</p>";
    } else {
        echo mysqli_error($conexion);
    }
    desconectar($conexion);
}

function existeUsuario($nombre_usuario) {
    $conexion = conectar("royal");
    $consulta = "select * from user where username='$nombre_usuario';";
    $resultado = mysqli_query($conexion, $consulta);
    $contador = mysqli_num_rows($resultado);
    desconectar($conexion);

    if ($contador == 0) {
        return false;
    } else {
        return true;
    }
}

function existeCarta($ncarta) {
    $conexion = conectar("royal");
    $consulta = "select * from card where name='$ncarta'";
    // Ejecutamos la consulta
    $resultado = mysqli_query($conexion, $consulta);
    // Contamos cuantas filas tiene el resultado
    $contador = mysqli_num_rows($resultado);
    desconectar($conexion);
    // Si devuelve 1 es que ya existe un usuario con ese nombre de usuario
    // Si devuelve 0 no existe
    if ($contador == 0) {
        return false;
    } else {
        return true;
    }
}

function selectAllUsers() {
    $con = conectar('royal');
    $select = 'select * from user';
    $resultado = mysqli_query($con, $select);
    desconectar($con);
    return $resultado;
}

function borrarUser($cp) {
    $con = conectar("royal");
    $delete = "delete from user where username= '$cp'";
    if (mysqli_query($con, $delete)) {
        echo "Usuario Borrado";
    } else {
        echo mysqli_error($con);
    }
    desconectar($con);
}
