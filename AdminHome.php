<html>
    <head>
        <meta charset="UTF-8">
        <title>Home Page Administrator</title>
    </head>
    <body>
        <?php
        session_start();
        // Nos aseguramos de que haya un usuario autentificado
        if (isset($_SESSION["username"])) {
            if ($_SESSION["type"] == 1) {
                // Cogemos la variable de sesión y saludamos al usuario
                $username = $_SESSION["username"];
                echo "<h2>Hola $username</h2>";
                ?>
                <p><a href="NewCard.php">Alta de Cartas</a></p>
                <p><a href="RankingUser.php">Ranking Mejores Usuarios</a></p>
                <p><a href="BorrarUser.php">Borrar un Usuario</a></p>
                <p><a href="AñadirCard.php">Añadir Carta a Usuario</a></p>
                <?php
            } else {
                echo "No eres administrador.";
            }
        } else {
            echo "No estás autentificado.";
        }
        ?>
    </body>
</html>
