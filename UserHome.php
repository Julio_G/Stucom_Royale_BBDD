<!DOCTYPE html>
<!-- HomePage del usuario -->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Pagina del usuario</title>
    </head>
    <body>
        <?php
        session_start();
        // Nos aseguramos de que haya un usuario autentificado
        if (isset($_SESSION["username"])) {
            // Cogemos la variable de sesión y saludamos al usuario
            $username = $_SESSION["username"];
            $passactual = $_SESSION["password"];
            echo "<h2>Hola - $username</h2>";
            ?>
            <p><a href="UpdateUser.php">Modificar Contraseña</a></p>
            <p><a href="DatosUser.php">Ver Perfil</a></p>

            <?php
        } else {
            echo "No estás autentificado.";
        }
        ?>
    </body>
</html>
