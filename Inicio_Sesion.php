<html>
    <head>
        <meta charset="UTF-8">
        <title>Registro</title>
    </head>
    <body>
        <form action="" method="POST">
            <p>Usuario: <input type="text" name="username"></p>
            <p>Password: <input type="password" name="password"></p>
            <input type="submit" name="login" value="Iniciar Sesion">
        </form>
        <?php
        require_once 'bbdduser.php';
        if (isset($_POST["login"])) {
            // Recogemos los datos del login
            $username = $_POST["username"];
            $pass = $_POST["password"];
            if (verificarUser($username, $pass)) { // verificarUser(..)==true
                // Guardar datos del usuario en variable de sesión
                session_start();
                $_SESSION["username"] = $username;
                $_SESSION["password"] = $passactual;
                $tipo = getTipoUsuario($username);
                $_SESSION["type"] = $tipo;
                if ($tipo == 0) {
                    // Dirigimos al usuario a su homePage.
                    echo "Hola";
                    header("Location: UserHome.php");
                } else if ($tipo == 1) {
                    // Dirigimos a la página de administrador
                    header("Location: AdminHome.php");
                } else { // Aquí no debería entrar nunca
                    echo "Tipo de usuario incorrecto";
                }
            } else {
                echo "Nombre de usuario o contraseña incorrectos";
            }
        }
        ?>
        <p><a href="index.php">Inicio</a></p>

    </body>
</html>