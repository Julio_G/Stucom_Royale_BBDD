<html>
    <head>
        <meta charset="UTF-8">
        <title>Nueva Carta</title>
    </head>
    <body>
        <form action="" method="POST">
            <p>Nombre de Carta: <input type="text" name="nombre"></p>
            <p>Tipo:<select name="tipo">
                    <option value="tropa">Tropa</option>
                    <option value="hechizo">Hechizo</option>
                    <option value="estructura">Estructura</option>
                </select></p>
            <p>Calidad: <select name="calidad">
                    <option value="comun">Comun</option>
                    <option value="especial">Especial</option>
                    <option value="epica">Epica</option>
                    <option value="legendaria">Legendaria</option>
                </select></p>
            <p>Salud:(1 - 20)<input type="number" name="salud"></p>
            <p>Daño:(1 - 20)<input type="number" name="daño"></p>
            <p>Coste Elixir:(1 - 10)<input type="number" name="coste"></p>
            <input type="submit" value="Registrar" name="alta">
        </form>
        <?php
        require_once 'bbdduser.php';
        // Si han pulsado el botón registramos el usuario
        if (isset($_POST["alta"])) {
            // Recogemos el nombre de usuario
            $ncarta = $_POST["nombre"];
            // Comprobamos si ya existe
            if (existeCarta($ncarta) == true) {
                echo "<p>Ya existe esa carta en la bbdd</p>";
            } else {
                // Recogemos el resto de datos
                $nombre = $_POST["nombre"];
                $tipo = $_POST['tipo'];
                $calidad = $_POST['calidad'];
                $salud = $_POST['salud'];
                $daño = $_POST['daño'];
                $coste = $_POST['coste'];                
                // Registramos el usuario en la bbdd
                insertCard($nombre, $tipo, $calidad, $salud, $daño, $coste);
            }
        }
        ?>
        <p><a href="index.php">Inicio</a></p>
    </body>
</html>
